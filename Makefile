all: server client 

server: server.c
	g++ -I. server.c -o server 

client: client.c
	g++ -I. client.c -o client 
	
clean:
	rm -rf *.o  client server
