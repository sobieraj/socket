#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 4455
 main(int argc, char* argv[]){
     int sockfd;
     struct sockaddr_in serverAddr;
  
     int newSocket;
     struct sockaddr_in newAddr;

     socklen_t addr_size;
     char buffer[1024];

     sockfd=socket(PF_INET, SOCK_STREAM, 0);
     printf("-> Server socket cerated  succesfull\n");
     memset(&serverAddr, '\0', sizeof(serverAddr));

         serverAddr.sin_family=AF_INET;
         serverAddr.sin_port=htons(PORT);
         serverAddr.sin_addr.s_addr=inet_addr("127.0.0.1");
 
     bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr)); 
     printf("-> Bind to the Port Number %d\n", 4455);
    
     listen(sockfd, 5);
      addr_size= sizeof(newAddr);
 

     newSocket= accept(sockfd, (struct sockaddr*)&newAddr, &addr_size);                   //potwierdzenie połączenia

     strcpy(buffer, "HELLO");
     send(newSocket, buffer, strlen(buffer), 0);

     printf("-> Send the Data tothe Client\n");
     printf("-> Closed the connection\n");
}